package apsPackage;

import java.util.ArrayList;

public class Banco {
	
	private ArrayList<ContaBancaria> contas = new ArrayList<ContaBancaria>();
	
	public Banco() {
		
	}
	
	public Banco(ArrayList<ContaBancaria> conta) {
		this.contas = conta;
	}

	public ArrayList<ContaBancaria> getContas() {
		return contas;
	}

	public void inserir(ContaBancaria objeto) {
		this.contas.add(objeto);
	}
	
	public void remover(long numero) {
		for(ContaBancaria cB : contas) {
			if(cB.getNumeroConta() == numero) {
				contas.remove(0);
			}
		}
	}
	
	/*
	public ContaBancaria procurarConta(long numeroConta) {
		
	}
	*/
	
}
