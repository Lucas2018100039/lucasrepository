package apsPackage;

public class ContaCorrente extends ContaBancaria{
	
	private double taxaDeOperacao;
	
	public ContaCorrente() {
		super();
	}

	public ContaCorrente(long numeroConta, double saldo, double taxaDeOperacao) {
		super(numeroConta, saldo);
		this.taxaDeOperacao = taxaDeOperacao;
	}

	public double getTaxaDeOperacao() {
		return taxaDeOperacao;
	}

	public void setTaxaDeOperacao(double taxaDeOperacao) {
		this.taxaDeOperacao = taxaDeOperacao;
	}

	@Override
	public void mostrarDados() {
		// TODO Auto-generated method stub
		
	}
	

}
