package apsPackage;

public abstract class ContaBancaria implements Imprimivel{
	
	private long numeroConta;
	private double saldo = 0;

	public ContaBancaria() {
		
	}
	
	public ContaBancaria(long numeroConta, double saldo) {
		this.numeroConta = numeroConta;
	}
	
	public long getNumeroConta() {
		return numeroConta;
	}

	public void sacar(double valor) {
		this.saldo = saldo - valor;
	}
	
	public void depositar(double valor) {
		this.saldo = saldo + valor;
	}
	
	public void transferir(double valor, ContaBancaria contaDe, ContaBancaria contaPara) {
		contaDe.saldo = saldo - valor;
		contaPara.saldo = saldo + valor;
	}
	
}
