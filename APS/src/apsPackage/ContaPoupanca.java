package apsPackage;

public class ContaPoupanca extends ContaBancaria{
	
	private double limite;
	
	public ContaPoupanca(){
		super();
	}

	public ContaPoupanca(long numeroConta, double saldo, double limite) {
		super(numeroConta, saldo);
		this.limite = limite;
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}

	@Override
	public void mostrarDados() {
		// TODO Auto-generated method stub
		
	}

}
