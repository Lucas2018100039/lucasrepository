package apsPackage;

import java.util.Scanner;

public class App {
	
	static Scanner sc = new Scanner(System.in);
	static Banco banco = new Banco();

	public static void main(String[] args) {

		System.out.println("1 - Cadastrar Conta:");
		System.out.println("2 - Sacar");
		System.out.println("3 - Depositar");
		System.out.println("4 - Transferir");
		System.out.print("Opcao: ");
		
		int opcao = sc.nextInt();
		
		if(opcao == 1) {
			cadastrarConta();
		}else if(opcao == 2) {
			sacar();
		}else if(opcao == 3) {
			depositar();
		}else {
			transferir();
		}
		
		sc.close();
	}
	
	public static void cadastrarConta(){
		System.out.print("Numero da conta: ");
		long numero = sc.nextLong();
		System.out.print("Saldo: ");
		double saldo = sc.nextDouble();
		System.out.print("Conta Poupanca ou Conta Corrente (cp/cr): ");
		String opcao = sc.nextLine();
		
		if(opcao == "cp") {
			System.out.print("Limite: ");
			double limite = sc.nextDouble();
			ContaPoupanca cp = new ContaPoupanca(numero,saldo,limite);
			banco.inserir(cp);
		}else {
			System.out.print("Taxa: ");
			double taxaDeOperacao = sc.nextDouble();
			ContaCorrente cr = new ContaCorrente(numero,saldo,taxaDeOperacao);
			banco.inserir(cr);
		}
	}
	
	public static void sacar() {
		System.out.print("Numero da Conta:");
		long numero = sc.nextLong();
		
	}
	
	public static void depositar() {
		
	}

	public static void transferir() {
		
	}
}
